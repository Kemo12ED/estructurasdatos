var RaizPadre = new Nodo(null,null,null,"Padre",0);
RaizPadre.HijoD= new Nodo(RaizPadre,null,null,"Hijo Derecho",1)
RaizPadre.HijoI= new Nodo(RaizPadre,null,null,"Hijo Izquierdo",1)
RaizPadre.HijoI.HijoD = new Nodo(RaizPadre.HijoI,null,null,'Hijo Izquierdo',2);
RaizPadre.HijoI.HijoI = new Nodo(RaizPadre.HijoI,null,null,'Hijo Derecho',2);
RaizPadre.HijoI.HijoD.HijoD = new Nodo(RaizPadre.HijoI.HijoD,null,null,'Hijo Derecho',3);
RaizPadre.HijoI.HijoD.HijoI = new Nodo(RaizPadre.HijoI.HijoD,null,null,'Hijo Izquierdo',3);

console.log(RaizPadre);